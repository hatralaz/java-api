package com.java.project.controller;

import com.java.project.auth.AuthenticationRequest;
import com.java.project.auth.AuthenticationResponse;
import com.java.project.config.JwtUtil;
import com.java.project.model.User;
import com.java.project.service.CustomUserDetails;
import com.java.project.service.UserDetailsServiceImpl;
import com.java.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;
    private final UserDetailsServiceImpl userDetailsService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private  UserService userService;

    public void verifyLogin(String username, String password) {
        User foundUser = userService.findByUserName(username).orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));;
        boolean isMatch = passwordEncoder.matches(password, foundUser.getPassword());
        if(!isMatch) {
            throw new AccessDeniedException("Wrong password"); // Throw an AccessDeniedException to return a 403
        }
    }

    public AuthenticationController(AuthenticationManager authenticationManager, JwtUtil jwtUtil, UserDetailsServiceImpl userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
        this.userDetailsService = userDetailsService;
    }

    @PostMapping("/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {

        verifyLogin(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        final CustomUserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        final String jwt = jwtUtil.generateToken(userDetails);
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<String> signup(@RequestBody User user) {
        // check if user already exists
        System.out.println("user" + user);
        Optional<User> existingUser = userService.findByUserName(user.getUsername());
        System.out.println(existingUser);
        if (existingUser.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Username already exists");
        }

        // save user to database
        userService.signUp(user);
        return ResponseEntity.status(HttpStatus.CREATED).body("User created successfully");
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<String> handleAccessDeniedException(AccessDeniedException e) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Access denied: " + e.getMessage());
    }
}