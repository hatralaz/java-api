package com.java.project.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.java.project.model.BaseModel;
import com.java.project.repository.BaseRepository;
import com.java.project.service.BaseService;
import com.java.project.service.CustomUserDetails;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
public abstract class BaseController<T extends BaseModel, R extends BaseRepository<T>, S extends BaseService<T,R>> {
//    protected static BaseOptions options;

    protected BaseService<T, R> baseService;

    public BaseController(BaseService<T, R> baseService) {
        this.baseService = baseService;
//        this.options = options;
    }


    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Optional<T>> getById(
            @PathVariable String id,
            @AuthenticationPrincipal CustomUserDetails userDetails
            ) {
        Optional<T> result = baseService.findById(id);
        if (result == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public Map<String, Object> getAllWithPagingation(
            @RequestParam(name = "where", required = false) Optional<String> whereParam,
            @RequestParam(name ="pageNumber", required = false) Optional<Integer> pageNumberParam,
            @RequestParam(name ="pageSize", required = false) Optional<Integer> pageSizeParam,
            @AuthenticationPrincipal CustomUserDetails userDetails
    ) throws JsonProcessingException {
        System.out.println(whereParam);
        int pageNumber = pageNumberParam.orElse(0);
        int pageSize = pageSizeParam.orElse(20);
        return baseService.findWithPagination( baseService.createQueryFromWhereParam(whereParam), pageNumber, pageSize);
    }

    @PostMapping
    public T create(
            @RequestBody T entity,
            @AuthenticationPrincipal CustomUserDetails userDetails
    ) {
        System.out.println(entity);
        System.out.println((entity.getCreatedAt()));
        return baseService.save(entity);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<T> update(
            @PathVariable String id,
            @RequestBody Map<String, Object> entity,
            @AuthenticationPrincipal CustomUserDetails userDetails
    ) {
        T result = baseService.updateById(id, entity);
        if (result == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Void> deleteById(
            @PathVariable String id,
            @AuthenticationPrincipal CustomUserDetails userDetails
            ) {
        boolean isDeleted = baseService.deleteById(id);
        if (!isDeleted) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping()
    @PreAuthorize("hasRole('ADMIN')")
    public long deleteByCondition(
            @RequestParam(name = "where", required = false) Optional<String> whereParam,
            @AuthenticationPrincipal CustomUserDetails userDetails
            ) throws JsonProcessingException {
        return baseService.deleteByCondition(baseService.createQueryFromWhereParam(whereParam));
    }
}

