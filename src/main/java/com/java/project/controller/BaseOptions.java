package com.java.project.controller;

public class BaseOptions {
    public final String basePath;
    public final Class<?> model;

    public BaseOptions(String basePath, Class<?> model) {
        this.basePath = basePath;
        this.model = model;
    }
}
