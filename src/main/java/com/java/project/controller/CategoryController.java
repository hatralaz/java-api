package com.java.project.controller;

import com.java.project.model.Category;
import com.java.project.repository.CategoryRepository;
import com.java.project.service.CategoryService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/categories")
@RestController
public class CategoryController extends BaseController<Category, CategoryRepository, CategoryService> {

    public CategoryController(CategoryService categoryService) {
        super(categoryService);
    }
}
