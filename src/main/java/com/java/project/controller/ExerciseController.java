package com.java.project.controller;

import com.java.project.model.Exercise;
import com.java.project.repository.ExerciseRepository;
import com.java.project.service.ExerciseService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/exercises")
@RestController
public class ExerciseController extends BaseController<Exercise, ExerciseRepository, ExerciseService> {

    public ExerciseController(ExerciseService exerciseService) {
        super(exerciseService);
    }
}
