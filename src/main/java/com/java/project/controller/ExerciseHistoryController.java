package com.java.project.controller;

import com.java.project.model.ExerciseHistory;
import com.java.project.repository.ExerciseHistoryRepository;
import com.java.project.service.CustomUserDetails;
import com.java.project.service.ExerciseHistoryService;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/exercises-history")
@RestController
public class ExerciseHistoryController extends BaseController<ExerciseHistory, ExerciseHistoryRepository, ExerciseHistoryService> {

    public ExerciseHistoryController(ExerciseHistoryService exerciseHistoryService) {
        super(exerciseHistoryService);
    }
    @PostMapping
    @Override
    public ExerciseHistory create(
            @RequestBody ExerciseHistory entity,
            @AuthenticationPrincipal CustomUserDetails userDetails
    ) {
        String userId = userDetails.getId();
        entity.setUserId(userId);
        return this.baseService.save(entity);
    }



//    @PostMapping
//    public void create(){
//        System.out.println("hihi");
//        return;
//    }

}
