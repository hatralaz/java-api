package com.java.project.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.java.project.model.ExerciseQuestion;
import com.java.project.model.Question;
import com.java.project.repository.ExerciseQuestionRepository;
import com.java.project.service.CustomUserDetails;
import com.java.project.service.ExerciseQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RequestMapping("/exercise-questions")
@RestController
public class ExerciseQuestionController extends BaseController<ExerciseQuestion, ExerciseQuestionRepository, ExerciseQuestionService> {

    public ExerciseQuestionController(ExerciseQuestionService exerciseQuestionService) {
        super(exerciseQuestionService);
    }
    @Autowired
    private ExerciseQuestionService exerciseQuestionService;

    @GetMapping("/get-exercise-questions/{exerciseId}")
//    @PreAuthorize("hasRole('ADMIN')")
    public Map<String, Object> getAllWithPagingation(
            @RequestParam(name = "where", required = false) Optional<String> whereParam,
            @RequestParam(name ="pageNumber", required = false) Optional<Integer> pageNumberParam,
            @RequestParam(name ="pageSize", required = false) Optional<Integer> pageSizeParam,
            @PathVariable String exerciseId,
            @AuthenticationPrincipal CustomUserDetails userDetails
    ) throws JsonProcessingException {
        System.out.println(exerciseId);
        int pageNumber = pageNumberParam.orElse(0);
        int pageSize = pageSizeParam.orElse(20);
        return exerciseQuestionService.getQuestionIdsByExerciseId( whereParam, pageNumber, pageSize, exerciseId);
    }

    @GetMapping("/get-exercise-questions-not-selected/{exerciseId}")
//    @PreAuthorize("hasRole('ADMIN')")
    public Map<String, Object> getAllNotSelectedWithPagingation(
            @RequestParam(name = "where", required = false) Optional<String> whereParam,
            @RequestParam(name ="pageNumber", required = false) Optional<Integer> pageNumberParam,
            @RequestParam(name ="pageSize", required = false) Optional<Integer> pageSizeParam,
            @PathVariable String exerciseId,
            @AuthenticationPrincipal CustomUserDetails userDetails
    ) throws JsonProcessingException {
        System.out.println(exerciseId);
        int pageNumber = pageNumberParam.orElse(0);
        int pageSize = pageSizeParam.orElse(20);
        return exerciseQuestionService.getQuestionIdsNotSelectedByExerciseId( whereParam, pageNumber, pageSize, exerciseId);
    }
}
