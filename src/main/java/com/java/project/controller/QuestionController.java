package com.java.project.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.java.project.model.Question;
import com.java.project.repository.QuestionRepository;
import com.java.project.service.CustomUserDetails;
import com.java.project.service.QuestionService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RequestMapping("/questions")
@RestController
public class QuestionController extends BaseController<Question, QuestionRepository, QuestionService> {

    public QuestionController(QuestionService questionService) {
        super(questionService);
    }
}
