package com.java.project.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.java.project.model.ExerciseHistory;
import com.java.project.model.User;
import com.java.project.repository.UserRepository;
import com.java.project.service.CustomUserDetails;
import com.java.project.service.ExerciseHistoryService;
import com.java.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.*;
import java.util.Optional;

@RequestMapping("/users")
@RestController
public class UserController extends BaseController<User, UserRepository, UserService>{

    @Autowired
    ExerciseHistoryService exerciseHistoryService;
    public UserController(UserService userService) {

        super(userService);
    }

    @GetMapping("/me")
    public Map<String, Object> getMe(
            @RequestParam(name = "where", required = false) Optional<String> whereParam,
            @RequestParam(name ="pageNumber", required = false) Optional<Integer> pageNumberParam,
            @RequestParam(name ="pageSize", required = false) Optional<Integer> pageSizeParam,
            @AuthenticationPrincipal CustomUserDetails userDetails
    ) throws JsonProcessingException {
        int pageNumber = pageNumberParam.orElse(0);
        int pageSize = pageSizeParam.orElse(20);
        List<Map<String, Object>> exerciseHistoryList = this.exerciseHistoryService.getHistory(userDetails.getId(), this.baseService.createQueryFromWhereParam((whereParam)) ,pageNumber, pageSize);
        Map<String, Object> response = new HashMap<>();
        response.put("userDetails", userDetails);
        response.put("exerciseHistory", exerciseHistoryList);
        return response;

    }

}
