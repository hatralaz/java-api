package com.java.project.model;

import java.lang.reflect.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import java.time.LocalDateTime;


public abstract class BaseModel {

    @Id
    protected String id;


    protected LocalDateTime createdAt;

    public BaseModel() {
        this.createdAt = LocalDateTime.now();
    }

    public String getId() {
        return id;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");

        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                Object value = field.get(this);
                sb.append(field.getName()).append(": ");
                if (value == null) {
                    sb.append("null, ");
                } else {
                    sb.append(value.toString()).append(", ");
                }
//                sb.append(field.getName() + ": " + field.get(this) + ", ");
            } catch (IllegalAccessException e) {
                // Handle exception
            }
        }

        //get field in subclass
        for (Field field : this.getClass().getSuperclass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                Object value = field.get(this);
                sb.append(field.getName()).append(": ");
                if (value == null) {
                    sb.append("null, ");
                } else {
                    sb.append(value.toString()).append(", ");
                }
//                sb.append(field.getName()).append(": ").append(field.get(this)).append(", ");
            } catch (IllegalAccessException e) {
                // Handle exception
                System.out.println(e);
            }
        }

        sb.setLength(sb.length() - 2); // Remove the last ", "
        sb.append("}");

        return sb.toString();
    }
}
