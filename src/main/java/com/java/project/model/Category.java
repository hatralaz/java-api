package com.java.project.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "categories")
public class Category extends BaseModel {
    private String cateName;

    public  Category() {}
    public Category(String cateName) {
        this.cateName = cateName;
    }

    public String getCateName() {
        return this.cateName;
    }
}
