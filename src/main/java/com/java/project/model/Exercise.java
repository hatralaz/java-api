package com.java.project.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "exercises")
public class Exercise extends BaseModel {
    private String name;
    private String description;
    private String categoryId;

    public Exercise() {}

    public Exercise(String name, String description, String categoryId) {
        this.name = name;
        this.description = description;
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
}