package com.java.project.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "exercise_history")
public class ExerciseHistory extends BaseModel {
    private String userId;
    private String exerciseId;
    private double result;

    public ExerciseHistory(){}
    public ExerciseHistory(String userId, String exerciseId, double result) {
        this.userId = userId;
        this.exerciseId = exerciseId;
        this.result = result;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getExerciseId() {
        return exerciseId;
    }

    public void setExerciseId(String exerciseId) {
        this.exerciseId = exerciseId;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }
}