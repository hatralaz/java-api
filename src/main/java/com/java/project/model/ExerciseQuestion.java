package com.java.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

@Document(collection = "exercise_questions")
public class ExerciseQuestion extends BaseModel {
    private String exerciseId;
    private String questionId;

    @JsonIgnore
    @Field("created_at")
    protected LocalDateTime createdAt;

    public ExerciseQuestion(String exerciseId, String questionId) {
        this.exerciseId = exerciseId;
        this.questionId = questionId;
    }

    public String getExerciseId() {
        return exerciseId;
    }

    public void setExerciseId(String exerciseId) {
        this.exerciseId = exerciseId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
}