package com.java.project.model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "questions")
public class Question extends BaseModel {
    private String description;
    private List<Option> options;

    public Question() {
        this.options = new ArrayList<>();
    }

    public Question(String description, List<Option> options) {
        this.description = description;
        this.options = options;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }
}

class Option {
    private String content;
    private boolean isTrue;

    public Option() {}

    public Option(String content, boolean isTrue) {
        this.content = content;
        this.isTrue = isTrue;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isTrue() {
        return isTrue;
    }

    public boolean getIsTrue() {
        return isTrue;
    }

    public void setIsTrue(boolean isTrue) {
        this.isTrue = isTrue;
    }
}