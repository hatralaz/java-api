package com.java.project.model;
import com.java.project.enums.UserType;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Document(collection = "users")
public class User extends BaseModel {
    private String name;
//    private int age;
    @Indexed(unique=true)
    private String userName;
    private String password;

    private UserType userType;

    public User(){}

    public User(String userName, String password) {
        this.userName = userName;
        this.password = new BCryptPasswordEncoder().encode(password);;
        this.userType = UserType.USER;
        this.name = userName;
    }

    public User(String userName, String password, String name) {
        this.userName = userName;
        this.password = new BCryptPasswordEncoder().encode(password);;
        this.userType = UserType.USER;
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

//    public void setAge(int age) {
//        this.age = age;
//    }
//
//    public int getAge() {
//        return age;
//    }

    public String getUsername() {
        return userName;
    }

    private void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    private void setPassword(String passWord) {
        this.password = passWord;
    }

    public UserType getAuthorities() {
        return userType;
    }
}
