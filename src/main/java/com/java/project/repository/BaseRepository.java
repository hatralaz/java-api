package com.java.project.repository;

import com.java.project.model.BaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BaseRepository<T extends BaseModel> extends MongoRepository<T, String> {
}
