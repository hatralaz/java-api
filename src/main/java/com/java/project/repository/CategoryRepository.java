package com.java.project.repository;

import com.java.project.model.Category;

public interface CategoryRepository extends BaseRepository<Category> {
}
