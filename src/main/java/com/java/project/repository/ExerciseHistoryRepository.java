package com.java.project.repository;

import com.java.project.model.ExerciseHistory;

public interface ExerciseHistoryRepository extends BaseRepository<ExerciseHistory> {
}
