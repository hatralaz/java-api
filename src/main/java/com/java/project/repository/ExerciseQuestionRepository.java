package com.java.project.repository;

import com.java.project.model.ExerciseQuestion;

public interface ExerciseQuestionRepository extends BaseRepository<ExerciseQuestion> {
}
