package com.java.project.repository;

import com.java.project.model.Exercise;

public interface ExerciseRepository extends BaseRepository<Exercise> {
}
