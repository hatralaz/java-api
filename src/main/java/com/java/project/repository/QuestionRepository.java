package com.java.project.repository;

import com.java.project.model.Question;

public interface QuestionRepository extends BaseRepository<Question> {
}
