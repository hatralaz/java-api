package com.java.project.repository;

import com.java.project.model.User;

public interface UserRepository extends BaseRepository<User>{
    User findByUsername(String username);
}
