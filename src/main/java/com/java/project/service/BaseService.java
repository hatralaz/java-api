package com.java.project.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.project.model.BaseModel;
import com.java.project.repository.BaseRepository;
import com.mongodb.client.result.DeleteResult;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.MongoTemplate;


@Service
public abstract class BaseService<T extends BaseModel, R extends BaseRepository<T>> {

    protected R baseRepository;
    protected Function<T, T> entityFactory;

    protected MongoTemplate mongoTemplate;
    protected abstract Class<T> getEntityClass();

    public BaseService(R baseRepository, MongoTemplate mongoTemplate, Function<T, T> entityFactory) {
        this.baseRepository = baseRepository;
        this.mongoTemplate = mongoTemplate;
        this.entityFactory = entityFactory;
    }
    public Optional<T> findById(String id) {
        return baseRepository.findById(id);
    }

    public List<T> findAll() {
        return baseRepository.findAll();
    }

    public Optional<T> findOne(Query query) {
        return Optional.ofNullable(mongoTemplate.findOne(query, getEntityClass()));
    }

    public Pageable createQueryWithPageAble(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return pageable;
    }

    public Query createQueryFromWhereParam(Optional<String> whereParam) throws JsonProcessingException {
        Query query = new Query();
        whereParam.ifPresent(where -> {
            try {
                Map<String, String> whereMap = new ObjectMapper().readValue(where, Map.class);
                whereMap.forEach((field, value) -> {
                    Criteria criteria = Criteria.where(field).is(value);
                    query.addCriteria(criteria);
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return query;
    }

    public Map<String, Object> findWithPagination(Query query, int pageNumber, int pageSize) {
        Pageable pageableRequest = createQueryWithPageAble(pageNumber, pageSize);
        query.with(pageableRequest);
//        System.out.println(mongoTemplate.count(query, getEntityClass()));
        long count = mongoTemplate.count(query, getEntityClass());
        List<T> data = mongoTemplate.find(query, getEntityClass());

        Map<String, Object> response = new HashMap<>();
        response.put("data", data);
        response.put("total", count);
        return response;
    }

    public List<T> findAllWithCondition(Query query) {
        return mongoTemplate.find(query, getEntityClass());
    }

    public T save(T entity) {
        T newEntity = entityFactory.apply(entity);
        return baseRepository.save(newEntity);
    }
    public boolean deleteById(String id) {
        baseRepository.deleteById(id);
        return false;
    }

    public long deleteByCondition(Query query) {
        DeleteResult result = mongoTemplate.remove(query, getEntityClass());
        return result.getDeletedCount();
    }

    public static Query build(Map<String, Object> where) {
        Query query = new Query();
        Criteria criteria = new Criteria();

        for (String key : where.keySet()) {
            criteria.and(key).is(where.get(key));
        }

        query.addCriteria(criteria);
        return query;
    }

    public boolean existsById(String id) {
        return baseRepository.existsById(id);
    }

    public T updateById(String id, Map<String, Object> fieldsToUpdate) {
        Query query = new Query(Criteria.where("_id").is(id));
        Update update = new Update();
        fieldsToUpdate.forEach(update::set);
        FindAndModifyOptions options = new FindAndModifyOptions().returnNew(true).upsert(false);
        T updatedEntity = mongoTemplate.findAndModify(query, update, options,getEntityClass());
        return updatedEntity;
    }
}
