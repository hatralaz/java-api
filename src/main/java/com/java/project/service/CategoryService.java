package com.java.project.service;

import com.java.project.model.Category;
import com.java.project.repository.CategoryRepository;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class CategoryService extends  BaseService<Category, CategoryRepository> {

    public CategoryService(CategoryRepository categoryRepository, MongoTemplate mongoTemplate) {
        super(categoryRepository, mongoTemplate, c -> new Category(c.getCateName()));
    }

    @Override
    public Class<Category> getEntityClass() {
        return Category.class;
    }
}
