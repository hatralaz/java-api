package com.java.project.service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.java.project.enums.UserType;
import com.java.project.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class CustomUserDetails implements UserDetails  {

    private final String id;

    private final String username;

    private final String name;

    private final UserType userType;

    private final String password;

    private final Set<GrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public CustomUserDetails(User user) {
        this.id = user.getId();
        this.userType = user.getAuthorities();
        this.password = user.getPassword();
        this.username = user.getUsername();
        this.authorities = new HashSet<>();
            authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getAuthorities().toString()));
            this.name = user.getName();
    }

    public String getId() {
        return id;
    }

    public UserType getUserType() {
        return userType;
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");

        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                Object value = field.get(this);
                sb.append(field.getName()).append(": ");
                if (value == null) {
                    sb.append("null, ");
                } else {
                    sb.append(value.toString()).append(", ");
                }
//                sb.append(field.getName() + ": " + field.get(this) + ", ");
            } catch (IllegalAccessException e) {
                // Handle exception
            }
        }

        //get field in subclass
        for (Field field : this.getClass().getSuperclass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                Object value = field.get(this);
                sb.append(field.getName()).append(": ");
                if (value == null) {
                    sb.append("null, ");
                } else {
                    sb.append(value.toString()).append(", ");
                }
//                sb.append(field.getName()).append(": ").append(field.get(this)).append(", ");
            } catch (IllegalAccessException e) {
                // Handle exception
                System.out.println(e);
            }
        }

        sb.setLength(sb.length() - 2); // Remove the last ", "
        sb.append("}");

        return sb.toString();
    }

    public String getName() {
        return this.name;
    }
}
