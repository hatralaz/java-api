package com.java.project.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.project.model.Exercise;
import com.java.project.model.ExerciseHistory;
import com.java.project.repository.ExerciseHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExerciseHistoryService extends  BaseService<ExerciseHistory, ExerciseHistoryRepository> {

    @Autowired
    ExerciseService exerciseService;
    public ExerciseHistoryService(ExerciseHistoryRepository exerciseHistoryRepository, MongoTemplate mongoTemplate) {
        super(exerciseHistoryRepository, mongoTemplate, exh -> new ExerciseHistory(exh.getUserId(), exh.getExerciseId(), exh.getResult()));
    }

    @Override
    public Class<ExerciseHistory> getEntityClass() {
        return ExerciseHistory.class;
    }

    public List<Map<String, Object>> getHistory(String userId, Query query, int pageNumber, int pageSize) {
        query.addCriteria(Criteria.where("userId").is(userId));
        Map<String, Object> exerciseHistories = this.findWithPagination(query, pageNumber, pageSize);
        List<ExerciseHistory> exerciseHistoryList = (List<ExerciseHistory>) exerciseHistories.get("data");

        List<Map<String, Object>> resultList = new ArrayList<>();
        for (ExerciseHistory exerciseHistory : exerciseHistoryList) {
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("exercise", this.exerciseService.findById(exerciseHistory.getExerciseId()));
            resultMap.put("result", exerciseHistory.getResult());
            resultList.add(resultMap);
        }
        return resultList;
    }
}
