package com.java.project.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.java.project.model.ExerciseQuestion;
import com.java.project.model.Question;
import com.java.project.repository.ExerciseQuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;

@Service
public class ExerciseQuestionService extends  BaseService<ExerciseQuestion, ExerciseQuestionRepository> {

    public ExerciseQuestionService(ExerciseQuestionRepository exerciseQuestionRepository, MongoTemplate mongoTemplate) {
        super(exerciseQuestionRepository, mongoTemplate, e -> new ExerciseQuestion(e.getExerciseId(), e.getQuestionId()));
    }

    @Autowired
    QuestionService questionService;

    @Override
    public Class<ExerciseQuestion> getEntityClass() {
        return ExerciseQuestion.class;
    }

    private static List<String> getQuestionIds(String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.readTree(jsonString);

        List<String> questionIds = new ArrayList<>();
        for (JsonNode dataNode : rootNode.path("data")) {
            String questionId = dataNode.path("questionId").asText();
            questionIds.add(questionId);
        }
        return questionIds;
    }

    public Map<String, Object> getQuestionIdsByExerciseId(Optional<String> whereParam, int pageNumber, int pageSize, String exerciseId) throws JsonProcessingException {
        Query query = this.createQueryFromWhereParam(whereParam);
        query.addCriteria(Criteria.where("exerciseId").is(exerciseId));
        Map<String, Object> questionExercise = this.findWithPagination(query, pageNumber, pageSize);

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(questionExercise);

        List<String> questionId = this.getQuestionIds(jsonString);
        Query queryQuestion = new Query(Criteria.where("id").in(questionId));
        List<Question> questions = questionService.findAllWithCondition(queryQuestion);

        Map<String, Object> response = new HashMap<>();
        response.put("data", questions);
        response.put("total", questionExercise.get("total"));

        return response;
    }

    public Map<String, Object> getQuestionIdsNotSelectedByExerciseId(Optional<String> whereParam, int pageNumber, int pageSize, String exerciseId) throws JsonProcessingException {
        Query query = this.createQueryFromWhereParam(whereParam);
        query.addCriteria(Criteria.where("exerciseId").is(exerciseId));
        Map<String, Object> questionExercise = this.findWithPagination(query, pageNumber, pageSize);

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(questionExercise);

        List<String> questionId = this.getQuestionIds(jsonString);
        Query queryQuestion = new Query(Criteria.where("id").nin(questionId));
        List<Question> questions = questionService.findAllWithCondition(queryQuestion);

        Map<String, Object> response = new HashMap<>();
        response.put("data", questions);
        response.put("total", questionExercise.get("total"));

        return response;
    }
}
