package com.java.project.service;

import com.java.project.model.Exercise;
import com.java.project.repository.ExerciseRepository;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class ExerciseService extends  BaseService<Exercise, ExerciseRepository> {

    public ExerciseService(ExerciseRepository exerciseRepository, MongoTemplate mongoTemplate) {
        super(exerciseRepository, mongoTemplate, e -> new Exercise(e.getName(), e.getDescription(), e.getCategoryId()));
    }

    @Override
    public Class<Exercise> getEntityClass() {
        return Exercise.class;
    }
}
