package com.java.project.service;

import com.java.project.model.Question;
import com.java.project.repository.QuestionRepository;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class QuestionService extends  BaseService<Question, QuestionRepository> {

    public QuestionService(QuestionRepository questionRepository, MongoTemplate mongoTemplate) {
        super(questionRepository, mongoTemplate, q -> new Question(q.getDescription(), q.getOptions()));
    }

    @Override
    public Class<Question> getEntityClass() {
        return Question.class;
    }
}
