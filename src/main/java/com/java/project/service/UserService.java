package com.java.project.service;

import com.java.project.model.User;
import com.java.project.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

@Service
public class UserService extends BaseService<User, UserRepository>{

    @Autowired
    private UserRepository userRepository;

    public UserService(UserRepository userRepository, MongoTemplate mongoTemplate) {
        super(userRepository, mongoTemplate, u -> new User(u.getUsername(), u.getPassword(), u.getName()));
    }

    public Optional<User> findByUserName(String userName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userName").is(userName));
        return Optional.ofNullable(mongoTemplate.findOne(query, User.class));
    }

    @Override
    public Class<User> getEntityClass() {
        return User.class;
    }

    public User signUp(User user) {
        User newUser = new User(user.getUsername(), user.getPassword());
        return userRepository.save(newUser);
    }

}
